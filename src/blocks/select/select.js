$('.select__input').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: Infinity,
    dropdownParent: select.closest('.select')
  });
});
